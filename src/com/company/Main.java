package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //*** Задание 1 ***
        Random rand = new Random();
        int eom[] = new int[10];
        int r = 50; // Предел генерации случайного значения (90 + не работает)
        for (int i = 0; i < 10; i++) {
//            Задаю случайные значения элементам массива
            eom[i] = rand.nextInt(r) + 1;
        }
        for (int i = eom.length - 1; i > 0; i--) {
//            Реализация пузырьковой сортировки по убыванию
            for (int j = 0; j < i; j++) {
//              Сравниваю элементы попарно, если они имеют неправильный порядок, то меняю местами
                if (eom[j] < eom[j + 1]) {
                    int p = eom[j];
                    eom[j] = eom[j + 1];
                    eom[j + 1] = p;
                }
            }
        }
        System.out.println();
        System.out.println("Сортировка по убыванию:");
        for (int i = 0; i < 10; i++) {
            System.out.print(eom[i]+" ");
        }
        for (int i = eom.length - 1; i > 0; i--) {
//            Реализация пузырьковой сортировки по возрастанию
            for (int j = 0; j < i; j++) {
//              Сравниваю элементы попарно, если они имеют неправильный порядок, то меняю местами
                if (eom[j] > eom[j + 1]) {
                    int p = eom[j];
                    eom[j] = eom[j + 1];
                    eom[j + 1] = p;
                }
            }
        }
        System.out.println();
        System.out.println();
        System.out.println("Сортировка по возрастанию:");
        for (int i = 0; i < 10; i++) {
            System.out.print(eom[i]+" ");
        }
        System.out.println();
        //*** Задание  2 ***
        int kol = r/11; //определяю количество возможных чисел со строгой последовательности цифр по возрастанию(для 90 + не работает)
        boolean check = false; //необходимо для прерывания циклов
        for (int i=0; i < 10; i++) {
            int chislo = 12; //переопределяю минимальное возможное число со строгой последовательностью цифр по возрастанию
            for (int j=0; j<kol; j++) {
                if (eom[i] == chislo) {
                    System.out.println();
                    System.out.println("Число массива со строгим порядком цифр это "+eom[i]+".");
                    check = true;
                    break;
                }
                chislo=chislo+11;
                if (check) break;
            }
        }
        if (!check) {
            System.out.println();
            System.out.println("Числа со строгим порядком цифр в массиве НЕТ");
        }
        //*** Задание 3 ***
        try {
            Scanner in = new Scanner(System.in);
            System.out.println();
            System.out.print("Введите номер месяца - ");
            String nomber = in.nextLine();
            int n  = Integer.parseInt(nomber); //если введено НЕ число выдает исключение
            switch (n) {
                case 1: System.out.println("Январь"); break;
                case 2: System.out.println("Февраль"); break;
                case 3: System.out.println("Март"); break;
                case 4: System.out.println("Апрель"); break;
                case 5: System.out.println("Май"); break;
                case 6: System.out.println("Июнь"); break;
                case 7: System.out.println("Июль"); break;
                case 8: System.out.println("Август"); break;
                case 9: System.out.println("Сентябрь"); break;
                case 10: System.out.println("Октябрь"); break;
                case 11: System.out.println("Ноябрь"); break;
                case 12: System.out.println("Декабрь"); break;
                default: System.out.println("Такого месяца нет");
            }
        }
        catch(NumberFormatException e){ //описание исключения
            System.out.println("Неверный формат ввода");
        }
        // *** Задание 4 ***
        System.out.println();
        int[][] array = new int[4][4]; //Объвление двумерного массива
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print((array[i][j] = (rand.nextInt(r) + 1)) + "\t"); //Генерация чисел и вывод
            }
            System.out.println();
        }
        int[] array2 = new int[16]; //Объявление одномерного массива для выполнения сортировки
        int k = 0;                  //Счетчик для перемещения по одномерному массиву
        for (int x1 = 0; x1 < 4; x1++) {
            for (int y1 = 0; y1 < 4; y1++) {
                array2[k] = array[x1][y1]; //Переписываю двумерный массив в одномерный
                k++;
            }
        }
        for (int i = 15; i > 0; i--) {
//           Реализация пузырьковой сортировки по возрастанию
           for (int j = 0; j < i; j++) {
//           Сравниваю элементы попарно, если они имеют неправильный порядок, то меняю местами
              if (array2[j] > array2[j + 1]) {
                  int p = array2[j];
                  array2[j] = array2[j + 1];
                  array2[j + 1] = p;
              }
           }
        }
        k=0;
        for (int x1 = 0; x1 < 4; x1++) {
            for (int y1 = 0; y1 < 4; y1++) {
                array[x1][y1]= array2[k] ; //Переписываю одномерный массив в двумерный
                k++;
            }
        }
        System.out.println();
        System.out.println("Сортировка по возрастанию:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        for (int i = 15; i > 0; i--) {
//           Реализация пузырьковой сортировки по убыванию
            for (int j = 0; j < i; j++) {
//           Сравниваю элементы попарно, если они имеют неправильный порядок, то меняю местами
                if (array2[j] < array2[j + 1]) {
                    int p = array2[j];
                    array2[j] = array2[j + 1];
                    array2[j + 1] = p;
                }
            }
        }
        k=0;
        for (int x1 = 0; x1 < 4; x1++) {
            for (int y1 = 0; y1 < 4; y1++) {
                array[x1][y1]= array2[k] ; //Переписываю одномерный массив в двумерный
                k++;
            }
        }
        System.out.println();
        System.out.println("Сортировка по убыванию:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        // *** Задание 5 ***
        System.out.println();
        array = new int[4][4]; //Объвление двумерного массива
        int maxi=0, maxj=0, max=0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print((array[i][j] = (rand.nextInt(r) + 1)) + "\t"); //Генерация чисел и вывод
                if (array[i][j] > max) {
                    max = array[i][j];
                    maxi = i;
                    maxj = j;
                }
            }
            System.out.println();
        }
        System.out.println();
        System.out.println(maxi+":"+maxj+" "+max); //определение максимального элемента массива
        int i=0, j=0;
        do {
            if ((i)>=maxi) {                    //сравнивает счетчик со строкой максимального элемента
                for (int d=0; d < 3; d++){
                    array[i][d] = array[i+1][d];    //заменяет текущую строчку следующей
                }
            }
            i++;
        } while (i<3);
        do {
            if ((j)>=maxj) {                    //сравнивает счетчик со столбцом максимального элемента
                for (int d=0; d < 3; d++) {
                    array[d][j] = array[d][j+1];    //заменяет текущий столбец следующим
                }
            }
            j++;
        } while (j<3);
        // Обнуление лишних столбцов и строк
        for (int b=0; b < 4; b++) {
            array[b][3] = 0;
        }
        for (int a=0; a < 4; a++) {
            array[3][a] = 0;
        }

        System.out.println();
        System.out.println("Матрица без столбеца и строки с максимальным элементом");
        for (int a = 0; a < 4; a++) {
            for (int b = 0; b < 4; b++) {
                System.out.print(array[a][b] + "\t");
            }
            System.out.println();
        }
    }
}
